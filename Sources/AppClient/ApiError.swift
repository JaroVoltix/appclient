//
//  File.swift
//  
//
//  Created by Jarosław Krajewski on 27/11/2020.
//

import Foundation

enum ApiError: Error {
    case parse(error: Error)
}

