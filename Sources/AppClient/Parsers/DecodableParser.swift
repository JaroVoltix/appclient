//
//  DecodableParser.swift
//  
//
//  Created by Jarosław Krajewski on 27/11/2020.
//

import Foundation


public class DecodableParser: Parser {
    public let decoder: JSONDecoder
    
    public init(decoder: JSONDecoder) {
        self.decoder = decoder
    }
    
    public func parse<T>(type: T.Type, data: Data) throws -> T {
            
        guard let decodableType = type as? Decodable.Type else {
            throw DecodableParserError.wrongType
        }
        
        guard let decoded = try decodableType.init(jsonData: data,decoder: decoder) as? T else {
            throw DecodableParserError.castingError
        }
        return decoded
    }
}

public enum DecodableParserError: Error {
    case wrongType
    case castingError
}

extension Decodable {
    
    init(jsonData: Data, decoder: JSONDecoder) throws {
          self = try decoder.decode(Self.self, from: jsonData)
    }
}
