//
//  File.swift
//  
//
//  Created by Jarosław Krajewski on 27/11/2020.
//

import Foundation


public protocol Parser {
    func parse<T>(type: T.Type, data: Data) throws -> T
}

