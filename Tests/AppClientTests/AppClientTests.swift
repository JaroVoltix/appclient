import XCTest
@testable import AppClient

final class AppClientTests: XCTestCase {
    
    func testExample() {
        XCTAssertEqual(AppClient().text, "Hello, World!")
    }
    
    func testDecodableParser() {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        let parser = DecodableParser(decoder: decoder)
        let data = test.data(using: .utf8)!
        let test = try! parser.parse(type: DecodableTest.self, data: data)
        XCTAssertEqual(test.name, "name")
        XCTAssertEqual(test.subtitle, "subtitle")
        XCTAssertEqual(test.date, dateFormatter.date(from: "2004-12-03"))
    }
    
    func testWrongTypeThrow() {
        let decoder = JSONDecoder()
        let parser = DecodableParser(decoder: decoder)
        let data = test.data(using: .utf8)!
        XCTAssertThrowsError(try parser.parse(type: NonDecodableTest.self, data: data)){ error in
            XCTAssertEqual(error as! DecodableParserError, DecodableParserError.wrongType)
        }
    }
    
    

    static var allTests = [
//        ("testExample", testExample),
        ("testDecodableParser", testDecodableParser),
        ("testWrongTypeThrow", testWrongTypeThrow),
    ]
}


let test = """
{
    "name": "name",
    "subtitle": "subtitle",
    "date": "2004-12-03"
}
"""

let test2 = """
{
    "name": "name",
    "subtitle": 1,
    "date": "2004-12-03"
}
"""


class DecodableTest: Decodable {
    let name: String
    let subtitle: String
    let date: Date
    
}


class NonDecodableTest {
    let name: String = ""
    let subtitle: String = ""
    let date: Date = Date()
    
}
