import XCTest

import AppClientTests

var tests = [XCTestCaseEntry]()
tests += AppClientTests.allTests()
XCTMain(tests)
